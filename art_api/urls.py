from django.conf.urls import *
from rest_framework import routers
from art_api import views


api = routers.DefaultRouter()
api.register(r'paintings', views.PaintingViewSet)

urlpatterns = patterns('',
                       url(r'^', include(api.urls)),
                       )
