from rest_framework import serializers
from art import models


class PaintingSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Painting
