from rest_framework import viewsets
from art import models
from art_api import serializers


class PaintingViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.PaintingSerializer
    queryset = models.Painting.objects.all()
