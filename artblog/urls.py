from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.views.generic import RedirectView, TemplateView


admin.autodiscover()

urlpatterns = patterns('',
                       # (r'^{{ project_name }}/', include('{{ project_name }}.foo.urls')),
                       (r'^api/?$', RedirectView.as_view(url='/api/1/')),
                       (r'^api/1/', include('art_api.urls')),
                       (r'^admin/', include(admin.site.urls)),
                       (r'^', include('art.urls')),
                       url(r'^$', TemplateView.as_view(template_name='home.html'), name='home'),
                       )

if settings.DEBUG:
    urlpatterns += patterns('',
                            (r'^media/(?P<path>.*)$', 'django.views.static.serve',
                             {'document_root': settings.MEDIA_ROOT}),
                            )
