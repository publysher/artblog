from artblog.settings import *  # noqa

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = ['localhost']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'artblog',
        'USER': 'artblog',
        'PASSWORD': 'artblog',
        'HOST': 'localhost',
    }
}

# noinspection PyUnresolvedReferences
MEDIA_ROOT = os.path.abspath(os.path.join(__file__, '..', '..', 'develop', 'media'))
MEDIA_URL = '/media/'

# noinspection PyUnresolvedReferences
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        },
        },
    'handlers': {
        'default': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'standard',
            },
        'request_handler': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'standard',
            },
        },
    'loggers': {

        '': {
            'handlers': ['default'],
            'level': 'DEBUG',
            'propagate': True
        },
        'django.request': {
            'handlers': ['request_handler'],
            'level': 'DEBUG',
            'propagate': False
        },
    }
}

COMPRESS_ENABLED = False
