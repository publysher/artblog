from artblog.settings import *  # noqa

DEBUG = False
TEMPLATE_DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'artblog',
        'USER': 'artblog',
        'PASSWORD': '8f9asf90shf3u037faosm874mxqimql87cn845nqwv5lqwn',
        'HOST': 'localhost',
        }
}

STATIC_ROOT = '/var/www/artblog_nl/static/'
STATIC_URL = 'http://static.artblog.publysher.nl/static/'
ADMIN_MEDIA_PREFIX = STATIC_URL + 'admin/'

MEDIA_ROOT = '/var/www/artblog_media/'
MEDIA_URL = 'http://media.artblog.publysher.nl/'

COMPRESS_ROOT = STATIC_ROOT

ALLOWED_HOSTS = ['.publysher.nl']
