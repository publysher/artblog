// composition lines over images
$(function() {

    var drawLine = function(ctx, x0, y0, x1, y1) {
        ctx.beginPath();
        ctx.moveTo(x0, y0);
        ctx.lineTo(x1, y1);
        ctx.stroke();
    };

    var drawComposition = function($canvas, src, type) {
        var img = new Image();
        img.onload = function() {
            var ctx = $canvas.get(0).getContext('2d'),
                width = img.width,
                height = img.height;

            $canvas.attr('width', width);
            $canvas.attr('height', height);
            ctx.drawImage(img, 0, 0);
            ctx.lineWidth = 1.5;
            ctx.strokeStyle = "silver";

            switch (type) {
                case 1: // rule-of-three
                    drawLine(ctx, 0, height/3.0, width, height/3.0);
                    drawLine(ctx, 0, height/1.5, width, height/1.5);
                    drawLine(ctx, width/3.0, 0, width/3.0, height);
                    drawLine(ctx, width/1.5, 0, width/1.5, height);
                    break;
                case 2: // main diagonals
                    drawLine(ctx, 0, 0, width, height);
                    drawLine(ctx, width, 0, 0, height);
                    break;
                case 3: // diagonal method
                    drawLine(ctx, 0, 0, width, width);
                    drawLine(ctx, width, 0, 0, width);
                    drawLine(ctx, 0, height, width, height-width);
                    drawLine(ctx, width, height, 0, height-width);
                    break;
                default:
                    alert("UNKNOWN COMPOSITION TYPE: " + type);
            }

        };
        img.src = src;
    };


    $("div[data-composition]").each(function() {
        var $container = $(this),
            $canvas = $("<canvas></canvas>"),
            img = new Image();

        if (!$canvas.get(0).getContext) {
            return;
        }

        drawComposition($canvas,
            $container.attr("data-composition-src"),
            parseInt($container.attr("data-composition")));

        $container.append($canvas).removeClass("hidden");
    });

});


// autosize snippets
$(function() {
    var findLargest = function($elements) {
        var largest = 0;
        $elements.each(function() {
            var $this = $(this);
            var h = $this.innerHeight();
            if (h > largest) {
                largest = h;
            }
        });
        return largest;
    };

    // find largest snippet-1
    var $snippet1 = $(".snippet-1 .snippet-body"),
        $snippet2 = $(".snippet-2 .snippet-body"),
        largest1 = findLargest($snippet1),
        largest2 = findLargest($snippet2);

    if (largest1 * 2 > largest2) {
        largest2 = largest1 * 2;
    }
    else {
        largest1 = largest2 / 2;
    }

    $snippet1.innerHeight(largest1);
    $snippet2.innerHeight(largest2);
});
