from django import template


register = template.Library()

@register.simple_tag(takes_context=True)
def active(context, prefix):
    if context['request'].path.startswith(prefix):
        return "active"
    return ""
