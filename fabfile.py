import datetime
from fabric.context_managers import cd
from fabric.operations import sudo
from fabric.state import env

PROJECT = "artblog.nl"

REPO = "ssh://git@bitbucket.org/publysher/artblog.git"
USER = "django"
NOW = datetime.datetime.now().strftime("%Y%m%d-%H%M")
APP_DIR = "/home/%s/%s/%s" % (USER, PROJECT, NOW)
LIVE_DIR = "/home/%s/%s/active" % (USER, PROJECT)

def vps01():
    env.hosts = ['yigal@vps01.publysher.nl']


def deploy():
    sudo("git clone %s %s" % (REPO, APP_DIR), user=USER)
    with cd(APP_DIR):
        sudo("python bootstrap.py", user=USER)
        sudo("bin/buildout -c buildout-prod.cfg", user=USER)
        sudo("bin/django syncdb", user=USER)
        sudo("bin/django migrate", user=USER)
        sudo("bin/django collectstatic --noinput", user=USER)

        sudo("rm -f %s" % (LIVE_DIR,))
        sudo("ln -s %s %s" % (APP_DIR, LIVE_DIR))

    sudo("service apache2 restart")
