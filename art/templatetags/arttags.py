from django import template
from django.template import loader
from django.template.context import RequestContext
from art import models


register = template.Library()

ICONS = {
    models.Event: 'icon-book',
    models.Person: 'icon-user',
    models.Symbol: 'icon-puzzle-piece',
}


def schema(context, obj, size, itemprop=None):
    cls = obj.__class__
    model = cls.__name__.lower()
    data = {
        'object': obj,
        'model': model,
        'size': size,
        'icon': ICONS.get(cls, ''),
        'itemprop': itemprop,
    }
    rc = RequestContext(request=context['request'])
    template = (
        'art/{0}-{1}.html'.format(model, size),
        'art/generic-{0}.html'.format(size),
    )

    return loader.render_to_string(template, data, context_instance=rc)


@register.simple_tag(takes_context=True)
def schema_full(context, obj):
    return schema(context, obj, 'full')


@register.simple_tag(takes_context=True)
def schema_summary(context, obj):
    return schema(context, obj, 'summary')


@register.simple_tag(takes_context=True)
def schema_thumb(context, obj):
    return schema(context, obj, 'thumb')


@register.simple_tag(takes_context=True)
def schema_inline(context, obj):
    return schema(context, obj, 'inline')


@register.simple_tag(takes_context=True)
def schema_aside(context, obj, itemprop):
    return schema(context, obj, 'aside', itemprop)


@register.filter()
def paragraphs(value, amount):
    print "START"
    index = 0
    for i in range(amount * 2):
        print index
        index = value.find('\n', index + 1)
        if index == -1:
            break

    if index <= 0:
        return value

    return value[0:index]
