from django.db.models import Count
from django.views.generic import DetailView, ListView
from art import models


class PaintingDetailView(DetailView):

    model = models.Painting


class PaintingListView(ListView):

    model = models.Painting


class PlaceDetailView(DetailView):
    
    model = models.Place
    
    
class SymbolDetailView(DetailView):

    model = models.Symbol


class SymbolListView(ListView):
    
    model = models.Symbol


class EventDetailView(DetailView):

    model = models.Event


class EventListView(ListView):

    model = models.Event


class PersonDetailView(DetailView):

    model = models.Person


class PersonListView(ListView):

    queryset = (models.Person.objects
                .annotate(num_paintings=Count('painting'))
                .filter(num_paintings__ge=1)
                .order_by("name"))


class ArtistListView(ListView):

    template_name = "art/artist_list.html"
    queryset = (models.Person.objects
                .annotate(num_paintings=Count('created_paintings'))
                .filter(num_paintings__ge=1)
                .order_by("name"))
