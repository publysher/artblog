Views
-----

Every model has four kinds of views:

* Full    (all primary data, used for detail views)
* Summary (most primary data, used for large archive views)
* Thumb   (used for thumbnail lists)
* Inline  (used for inline references, usually in lists)



Generic Structure
-----------------

Main
----
- Title
- Image
- Description (short text)
- Optional: long text

Related
-------

Meta - Object
-------------
- Schema data

Meta - Post
-----------
- Read more
- Permalink
- Published
- Modified




