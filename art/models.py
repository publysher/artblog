import os
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.text import slugify
import django_countries
from django_extensions.db import fields
from imagekit.models import ImageSpecField
from pilkit.processors import SmartResize, ResizeToCover


def generic_upload(instance, filename):
    _, extension = os.path.splitext(filename)
    dirname = instance.__class__.__name__.lower()
    return "{0}/{1}/{2}{3}".format(dirname, instance.uuid, slugify(instance.name), extension)


class Thing(models.Model):

    class Meta:
        abstract = True

    schema = "http://schema.org/Thing"

    uuid = fields.UUIDField(version=4, primary_key=True)
    slug = fields.AutoSlugField(populate_from='name')
    created = fields.CreationDateTimeField()
    last_modified = fields.ModificationDateTimeField()

    name = models.CharField(max_length=300)
    description = models.TextField(blank=True, null=True)
    url = models.URLField(blank=True, null=True)

    image = models.ImageField(upload_to=generic_upload, max_length=300, null=True, blank=True)
    image_detail = ImageSpecField([ResizeToCover(570, 1)], source='image')
    image_summary = ImageSpecField([SmartResize(370, 370 / 1.61803398875)], source='image')
    image_thumbnail = ImageSpecField([SmartResize(150, 150)], source='image')

    sources = generic.GenericRelation('Source')

    def __unicode__(self):
        return u"{0}".format(self.name)

    def get_absolute_url(self):
        view = self.__class__.__name__.lower() + "-detail"
        return reverse(view, args=(self.slug,))


class Source(Thing):

    schema = "http://schema.org/Thing"

    content_type = models.ForeignKey(ContentType)
    object_id = fields.UUIDField()
    parent = generic.GenericForeignKey('content_type', 'object_id')


class Quote(Thing):

    schema = "http://schema.org/Thing"

    content_type = models.ForeignKey(ContentType)
    object_id = fields.UUIDField()
    parent = generic.GenericForeignKey('content_type', 'object_id')


class Address(Thing):

    schema = "http://schema.org/Address"

    street_address = models.CharField(max_length=300, null=True, blank=True)
    post_office_box_number = models.CharField(max_length=300, null=True, blank=True)
    postal_code = models.CharField(max_length=300, null=True, blank=True)
    address_locality = models.CharField(max_length=300)
    address_region = models.CharField(max_length=300, null=True, blank=True)
    address_country = django_countries.CountryField()

    def __unicode__(self):
        return u"{0}, {1}, {2}".format(self.street_address, self.address_locality, self.address_country)


class Place(Thing):

    schema = "http://schema.org/Place"

    address = models.ForeignKey(Address, blank=True, null=True)


class CreativeWork(Thing):

    class Meta:
        abstract = True

    date_created = models.CharField(max_length=100, blank=True, null=True)


class Painting(CreativeWork):

    schema = "http://schema.org/Painting"

    CATEGORY_CHOICES = (
        (0, 'Unknown'),
        (1, 'Bible'),
        (3, 'History'),
        (4, 'Mythology'),
    )

    TECHNIQUE_CHOICES = (
        (0, 'Unknown'),
        (1, 'Oil on Panel'),
    )

    category = models.PositiveSmallIntegerField(choices=CATEGORY_CHOICES, default=0)
    technique = models.PositiveSmallIntegerField(choices=TECHNIQUE_CHOICES, default=0)

    author = models.ForeignKey('Person', related_name='created_paintings')
    content_location = models.ForeignKey('Place', null=True, blank=True)

    quotes = generic.GenericRelation('Quote')

    about_persons = models.ManyToManyField('Person', blank=True)
    about_events = models.ManyToManyField('Event', blank=True)
    mentions_symbols = models.ManyToManyField('Symbol', blank=True)


class Event(Thing):

    schema = "http://schema.org/Event"

    attendees = models.ManyToManyField(to='Person', blank=True)
    start_date = models.CharField(max_length=300, null=True, blank=True)
    end_date = models.CharField(max_length=300, null=True, blank=True)


class Person(Thing):

    schema = "http://schema.org/Person"

    birth_date = models.CharField(max_length=300, null=True, blank=True)
    death_date = models.CharField(max_length=300, null=True, blank=True)


class Symbol(Thing):

    schema = "http://schema.org/Thing"
