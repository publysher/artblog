from django.contrib import admin
from django.contrib.contenttypes import generic
from imagekit.admin import AdminThumbnail
from art import models


class SourceInline(generic.GenericTabularInline):
    model = models.Source
    extra = 1
    fields = ('name', 'url')


class QuoteInline(generic.GenericTabularInline):
    model = models.Quote
    extra = 1
    fields = ('name', 'description', 'url')


class PaintingAdmin(admin.ModelAdmin):

    list_display = ('name', 'author', 'admin_thumbnail')
    admin_thumbnail = AdminThumbnail(image_field='image_thumbnail')
    inlines = [
        QuoteInline,
        SourceInline,
    ]
    filter_horizontal = ['about_persons', 'about_events', 'mentions_symbols']


class PersonAdmin(admin.ModelAdmin):

    list_display = ('name', )
    inlines = [
        SourceInline,
    ]


class EventAdmin(admin.ModelAdmin):

    list_display = ('name', )
    inlines = [
        SourceInline,
    ]


class SymbolAdmin(admin.ModelAdmin):

    list_display = ('name', )
    inlines = [
        SourceInline,
    ]


admin.site.register(models.Painting, PaintingAdmin)
admin.site.register(models.Event, EventAdmin)
admin.site.register(models.Person, PersonAdmin)
admin.site.register(models.Symbol, SymbolAdmin)
admin.site.register(models.Address)
admin.site.register(models.Place)
admin.site.register(models.Quote)
