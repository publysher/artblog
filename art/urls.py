from django.conf.urls import *

# place app url patterns here
from art import views


urlpatterns = patterns('',
                       url(r'^paintings/(?P<slug>[-\w]+)$', views.PaintingDetailView.as_view(), name='painting-detail'),
                       url(r'^paintings/$', views.PaintingListView.as_view(), name='painting-listview'),

                       url(r'^artists/$', views.ArtistListView.as_view(), name='artist-listview'),

                       url(r'^places/(?P<slug>[-\w]+)$', views.PlaceDetailView.as_view(), name='place-detail'),

                       url(r'^articles/symbols/(?P<slug>[-\w]+)$', views.SymbolDetailView.as_view(), name='symbol-detail'),
                       url(r'^articles/symbols/$', views.SymbolListView.as_view(), name='symbol-listview'),

                       url(r'^articles/events/(?P<slug>[-\w]+)$', views.EventDetailView.as_view(), name='event-detail'),
                       url(r'^articles/events/$', views.EventListView.as_view(), name='event-listview'),

                       url(r'^articles/persons/(?P<slug>[-\w]+)$', views.PersonDetailView.as_view(), name='person-detail'),
                       url(r'^articles/persons/$', views.PersonListView.as_view(), name='person-listview'),
                       )
